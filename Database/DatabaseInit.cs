using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Models;

namespace WebApp.Database
{
    public class DatabaseInit
    {
        public static void INIT(IServiceProvider serviceProvider){
            var context = new DatabaseContext(serviceProvider.GetRequiredService<DbContextOptions<DatabaseContext>>());
            context.Database.EnsureCreated();
            InsertData(context);
        }
        private static void InsertData(DatabaseContext database){
            if(database.customers.Any()) return;
            database.customers.Add(new Customer{
                username = "admin",
                password = "1234"
            });
            database.SaveChanges();
        }
    }
}