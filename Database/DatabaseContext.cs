﻿using Microsoft.EntityFrameworkCore;
using WebApp.Models;

namespace WebApp
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }
        public DbSet<Book> books { get; set; }
        public DbSet<Customer> customers { get; set; }
    }
}
