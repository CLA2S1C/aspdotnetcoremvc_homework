using System;
using System.Linq;
using WebApp.Models;

namespace WebApp.Services
{
    public interface IAuthService
    {
        Boolean Auth(Customer customer);
    }
    public class AuthService : IAuthService
    {
        private readonly DatabaseContext db;
        public AuthService(DatabaseContext _db){
            db = _db;
        }
        public Boolean Auth(Customer customer){
            return db.customers.Where(s => s.username == customer.username && s.password == customer.password).Any();
        }
    }
}