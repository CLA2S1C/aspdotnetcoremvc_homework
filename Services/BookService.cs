using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApp.Models;

namespace WebApp.Services
{
    public interface IBookService
    {
        Task<IEnumerable<Book>> SearchTxt(string txt);
        Boolean NewData(Book book);
        Boolean Edit(int id);
        Book EditData(int id);
        Boolean BookEdit(Book book);
        void RemoveBook(int id);
    }
    public class BookService : IBookService
    {
        private readonly DatabaseContext db;
        public BookService(DatabaseContext _db)
        {
            db = _db;
        }
        public async Task<IEnumerable<Book>> SearchTxt(string txt)
        {
            if (!String.IsNullOrEmpty(txt)){
                txt = txt.ToLower();
                return db.books.AsEnumerable().Where(s => s.Price.ToString().ToLower().Contains(txt) || s.Title.ToLower().Contains(txt) || s.ISBN.ToLower().Contains(txt)).ToList();
            }
            return await db.books.ToListAsync();
        }
        public Boolean NewData(Book book)
        {
            if (!String.IsNullOrEmpty(book.ISBN) && !String.IsNullOrEmpty(book.Title) && book.Price != 0)
            {
                db.books.AddAsync(new Book
                {
                    ISBN = book.ISBN.Trim(),
                    Title = book.Title.Trim(),
                    Price = book.Price
                });
                db.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public Boolean Edit(int id)
        {
            var searchEdit = db.books.Where(c => c.ID == id).Any();
            return searchEdit;
        }
        public Book EditData(int id){
            return db.books.Where(s => s.ID == id).FirstOrDefault();
        }
        public Boolean BookEdit(Book book)
        {
            try
            {
                var update = db.books.Where(s => s.ID == book.ID).FirstOrDefault();
                update.ISBN = book.ISBN;
                update.Title = book.Title;
                update.Price = book.Price;
                db.books.Update(update);
                db.SaveChanges();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        public void RemoveBook(int id){
            db.books.Remove(db.books.Where(c => c.ID == id).First());
            db.SaveChanges();
        }
    }
}