﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Customer
    {
        [Key]
        public int ID { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
