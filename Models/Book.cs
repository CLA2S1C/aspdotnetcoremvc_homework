﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Book
    {
        [Key]
        public int ID { get; set; }
        public string ISBN { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
    }
}
