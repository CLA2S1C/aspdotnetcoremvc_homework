// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.
// Write your JavaScript code.

$(document).ready(function () {
    if(window.localStorage.getItem('Token')) window.location.href = '/Book/Index';
    $('#Auth').click(function (e) { 
        e.preventDefault();
        let username = $('#username').val();
        let password = $('#password').val();
        if(!!username && !!password){
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Home/Auth",
                data: JSON.stringify({username, password}),
                dataType: "json",
                success: function (response) {
                    if(response.code === 200){
                        localStorage.setItem('Token', response.token);
                        window.location.href = '/Book/Index';
                    }else{alert("ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง")}
                }
            });
        }
    });
});