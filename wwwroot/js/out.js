$(function () {
    $(`#logout`).click(function (e) { 
        e.preventDefault();
        window.localStorage.clear();
        window.location.href = '/Home/Index';
    });
});