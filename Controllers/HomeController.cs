﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAuthService auth;

        public HomeController(IAuthService _auth)
        {
            auth = _auth;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Auth([FromBody]Customer customer)
        {
            if(auth.Auth(customer)) return Json(new {code = 200,token = customer.username});
            return Json(new {code = 400});
        }
    }
}
