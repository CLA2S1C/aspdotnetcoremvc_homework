﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using WebApp.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookService book;
        public BookController(IBookService _book){
            book = _book;
        }
        public async Task<IActionResult> Index(string txt)
        {
            ViewBag.Txt = txt;
            return View(await book.SearchTxt(txt));
        }
        public IActionResult Add(){
            return View();
        }
        [HttpPost]
        public IActionResult NewData(Book b){
            book.NewData(b);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Edit(int id){
            if(book.Edit(id)){
                var add = book.EditData(id);
                return View(add);
            }
            return View("Index");
        }
        [HttpPost]
        public IActionResult BookEdit(Book mbook){
            if(book.Edit(mbook.ID)) book.BookEdit(mbook);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult DeleteBook(int id){
            if(book.Edit(id)) book.RemoveBook(id);
            return RedirectToAction("Index");
        }
    }
}